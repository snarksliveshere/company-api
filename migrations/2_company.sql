-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TYPE company_type AS ENUM ('Corporations','NonProfit','Cooperative','Proprietorship');

CREATE TABLE IF NOT EXISTS company
(
    id               UUID
        CONSTRAINT group_pkey
            PRIMARY KEY                 NOT NULL,
    name             VARCHAR(15) UNIQUE NOT NULL,
    description      VARCHAR(3000)      NOT NULL DEFAULT '',
    amount_employees INT                NOT NULL,
    registered       BOOL               NOT NULL,
    type             company_type       NOT NULL
);

SELECT add_time_fields('company');

CREATE UNIQUE INDEX company_unique ON company (name);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE IF EXISTS company;