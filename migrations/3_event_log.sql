-- +goose Up
-- SQL in this section is executed when the migration is applied.

CREATE TABLE IF NOT EXISTS event_log
(
    id          BIGSERIAL PRIMARY KEY,
    action      TEXT        NOT NULL,
    entity_name TEXT        NOT NULL,
    entity_id   TEXT        NOT NULL,
    data        JSONB NOT NULL DEFAULT '{}',
    produced    BOOLEAN     NOT NULL DEFAULT FALSE,
    created_at  TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE IF EXISTS event_log;