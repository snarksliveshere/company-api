-- +goose Up
-- SQL in this section is executed when the migration is applied.

-- +goose StatementBegin

CREATE OR REPLACE FUNCTION update_timestamp()
    RETURNS TRIGGER AS
'
    BEGIN
        NEW.updated_at = now();
        RETURN NEW;
    END;
'
    LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_time_fields(table_name text) RETURNS VOID
    LANGUAGE plpgsql
    AS
        $$
DECLARE
    trigger_name TEXT;
BEGIN
    EXECUTE 'ALTER TABLE ' || table_name || ' ADD COLUMN created_at timestamp WITH TIME ZONE DEFAULT NOW() NOT NULL;';
    EXECUTE 'ALTER TABLE ' || table_name || ' ADD COLUMN updated_at timestamp WITH TIME ZONE DEFAULT NOW() NOT NULL;';
    trigger_name := 't_' || replace(table_name, '.', '_') || '_upt';

    EXECUTE 'CREATE TRIGGER ' || trigger_name || ' BEFORE UPDATE ON ' || table_name ||
            ' FOR EACH ROW EXECUTE PROCEDURE update_timestamp()';
END;
$$;
-- +goose StatementEnd

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP FUNCTION update_timestamp();
DROP FUNCTION add_time_fields(table_name text);