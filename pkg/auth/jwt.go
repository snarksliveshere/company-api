package auth

import (
	"fmt"

	"github.com/dgrijalva/jwt-go"
)

type tokenClaims interface {
	jwt.Claims
	IsClaimEmpty() bool
}

func DecodeJWT(t, encKey string, claims tokenClaims) error {
	token, err := jwt.ParseWithClaims(t, claims, func(token *jwt.Token) (interface{}, error) {
		if token.Method != jwt.SigningMethodHS256 {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(encKey), nil
	})
	if err != nil {
		return err
	}

	tc := token.Claims
	if token.Valid {
		if claims.IsClaimEmpty() {
			return fmt.Errorf("empty claim")
		}
		return nil
	} else {
		return fmt.Errorf("invalid claim: %+v", tc)
	}
}

func CreateJWT(c jwt.Claims, encKey string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, c)

	return token.SignedString([]byte(encKey))
}
