package server

//TODO: this package should be in the platform libraries

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type (
	HTTPServer struct {
		Port          int
		ReadTimeout   time.Duration
		WriteTimeout  time.Duration
		ShutdownDelay time.Duration
	}
	App struct {
		log               zerolog.Logger
		httpServer        *fiber.App
		httpAddr          string
		httpShutdownDelay time.Duration
		initRouterFn      func(r *fiber.App)
	}
)

// NewServer initialize new platform server configuration
func NewServer(log zerolog.Logger, cfg *HTTPServer) *App {
	a := &App{
		log: log,
		httpServer: fiber.New(fiber.Config{
			DisableKeepalive: true,
			ReadTimeout:      cfg.ReadTimeout,
			WriteTimeout:     cfg.WriteTimeout,
		}),
		httpAddr:          fmt.Sprintf(":%d", cfg.Port),
		httpShutdownDelay: cfg.ShutdownDelay,
		initRouterFn:      func(r *fiber.App) {},
	}

	return a
}

// Run server
func (a *App) Run() {
	a.registerDefaultHandlers()
	a.runHTTP()
}

func (a *App) runHTTP() {
	log.Info().Msg("HTTP Server is starting...")

	go func(a *App) {
		a.log.Info().Msgf("Starting HTTP Server on:%s", a.httpAddr)
		if err := a.httpServer.Listen(a.httpAddr); err != nil {
			a.log.Fatal().Msg(err.Error())
		}
	}(a)
	a.terminate()
}

func (a *App) terminate() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGTERM, syscall.SIGINT)

	oscall := <-c
	a.log.Info().Msgf("HTTPServer is terminating with system call: %+v...", oscall)

	baseContext := context.Background()
	ctxShutDown, cancel := context.WithTimeout(baseContext, a.httpShutdownDelay)
	defer func() {
		cancel()
	}()

	<-ctxShutDown.Done()

	if err := a.httpServer.ShutdownWithContext(baseContext); err != nil {
		a.log.Fatal().Msgf("HTTPServer shutdown failed, err:%v", err)
	}

	a.log.Info().Msgf("HTTPServer stopped with delay(ms):%d", a.httpShutdownDelay.Milliseconds())
}

func (a *App) SetupRouter(fn func(r *fiber.App)) {
	a.initRouterFn = fn
}

func (a *App) registerDefaultHandlers() {
	server := a.httpServer

	server.Get("/health", health())

	a.initRouterFn(server)
}

func health() func(*fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		return c.SendStatus(http.StatusOK)
	}
}
