package server

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"github.com/rs/zerolog"

	"github.com/dgrijalva/jwt-go"

	"gitlab.com/snarksliveshere/company-api/pkg/auth"
	"gitlab.com/snarksliveshere/company-api/pkg/errors"
)

const (
	HeaderXToken = "X-Token"
)

// User I get this from some kind of authentication server
type User struct {
	Name          string
	Surname       string
	Email         string
	Authenticated bool //TODO: The authentication server can pass different users
}

type UserClaim struct {
	*User
	jwt.StandardClaims
}

func (c UserClaim) IsClaimEmpty() bool {
	return c.User == nil
}

// AuthMiddleware check token
func AuthMiddleware(log zerolog.Logger, encKey string) fiber.Handler {
	return func(c *fiber.Ctx) error {
		token := c.GetReqHeaders()[HeaderXToken]
		if token == "" {
			log.Error().Msg("token does not exist")
			return c.Status(fiber.StatusForbidden).JSON(errors.ConvertToJSONErr(fmt.Errorf("token does not exist")))
		}
		cc := UserClaim{}

		err := auth.DecodeJWT(token, encKey, &cc)
		if err != nil {
			log.Error().Err(err)
			return c.Status(fiber.StatusForbidden).
				JSON(errors.ConvertToJSONErr(fmt.Errorf("cant decode JWT, because:%v", err)))
		}

		if !cc.Authenticated {
			return c.Status(fiber.StatusForbidden).JSON(errors.ConvertToJSONErr(fmt.Errorf("user is not authenticated")))
		}

		return c.Next()
	}
}
