package eventlog

import (
	sq "github.com/Masterminds/squirrel"
)

const (
	tblEventLog = "event_log"

	fieldAction     = "action"
	fieldEntityName = "entity_name"
	fieldEntityID   = "entity_id"
	fieldData       = "data"
)

// Record struct for creating save log query
type Record struct {
	Action     string
	EntityName string
	EntityID   string
	Data       string
}

// GetSaveEventLogQuery prepares sql insert builder
func GetSaveEventLogQuery(record *Record) sq.InsertBuilder {
	columns := make([]string, 0, 8)
	values := make([]interface{}, 0, 8)

	allFields := map[string]string{
		fieldAction:     record.Action,
		fieldEntityName: record.EntityName,
		fieldEntityID:   record.EntityID,
		fieldData:       record.Data,
	}

	for column, value := range allFields {
		if value != "" {
			columns = append(columns, column)
			values = append(values, value)
		}
	}

	return sq.
		Insert(tblEventLog).
		Columns(columns...).
		Values(values...).
		PlaceholderFormat(sq.Dollar)
}
