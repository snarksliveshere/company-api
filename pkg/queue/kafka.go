package queue

import (
	"fmt"

	"github.com/Shopify/sarama"
)

type (
	syncProducer struct {
		producer sarama.SyncProducer
	}

	Message struct {
		Key   string
		Value []byte
	}
)

// Queue provide access to producer
type Queue interface {
	// SendMessageInTopics send message, returns error
	SendMessageInTopics(topic string, message Message) error
}

// NewSyncProducer create new kafka sync producer
func NewSyncProducer(client sarama.Client) (*syncProducer, error) {
	producer, err := sarama.NewSyncProducerFromClient(client)
	if err != nil {
		return nil, err
	}

	return &syncProducer{
		producer: producer,
	}, nil
}

func (q syncProducer) SendMessageInTopics(topic string, message Message) error {
	if len(topic) == 0 {
		return fmt.Errorf("topic must exists")
	}
	return q.send(topic, append([]Message(nil), message))
}

func (q syncProducer) send(topic string, messages []Message) error {
	for _, m := range messages {
		el := &sarama.ProducerMessage{
			Topic: topic,
			Key:   sarama.StringEncoder(m.Key),
			Value: sarama.StringEncoder(m.Value),
		}
		_, _, err := q.producer.SendMessage(el)
		if err != nil {
			return fmt.Errorf("cant send msg to topic:%s with value:%s cause: %v", topic, string(m.Value), err)
		}
	}
	return nil
}
