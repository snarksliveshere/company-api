package pg

import (
	"context"
	"fmt"

	"github.com/georgysavva/scany/v2/pgxscan"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
)

// DBConn I need this to decorate the query and use transactions
type DBConn interface {
	pgxscan.Querier
	Exec(ctx context.Context, sql string, arguments ...any) (pgconn.CommandTag, error)
	Begin(ctx context.Context) (pgx.Tx, error)
}

// TransactionWrapper helper to use transactions
func TransactionWrapper(
	ctx context.Context,
	conn DBConn,
	wrappedFunc func(ctx context.Context, tx pgx.Tx) error,
) error {
	tx, err := conn.Begin(ctx)
	if err != nil {
		return err
	}
	if err = wrappedFunc(ctx, tx); err != nil {
		if rollbackErr := tx.Rollback(ctx); rollbackErr != nil {
			return fmt.Errorf("rollback error: %v", rollbackErr)
		}
		return err
	}
	if err = tx.Commit(ctx); err != nil {
		return err
	}
	return nil
}
