package errors

import (
	"errors"

	"github.com/gofiber/fiber/v2"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
)

//TODO: should be in lib

const (
	PGCodeInvalidInputValue  = "22P02"
	PGCodeDuplicateUnique    = "23505"
	PGCodeNullValueInNotNull = "23502"
)

func GetStatusCodeByPGErr(err error) (int, error) {
	if errors.Is(err, pgx.ErrNoRows) {
		return fiber.StatusNotFound, err
	}
	pgErr, ok := err.(*pgconn.PgError)
	if !ok {
		return 0, nil
	}
	switch pgErr.Code {
	case PGCodeInvalidInputValue, PGCodeNullValueInNotNull, PGCodeDuplicateUnique:
		return fiber.StatusBadRequest, err
	default:
		return fiber.StatusInternalServerError, err
	}
}

func ConvertToJSONErr(err error) fiber.Map {
	return fiber.Map{"error": err.Error()}
}
