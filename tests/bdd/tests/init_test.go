//go:build bdd_tests

package bdd_tests

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"testing"

	"github.com/cucumber/godog"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/snarksliveshere/kafka-batch-lib/consumer"

	"gitlab.com/snarksliveshere/company-api/internal/app/api/v1/company"
	"gitlab.com/snarksliveshere/company-api/internal/bootstrap"
	"gitlab.com/snarksliveshere/company-api/tests/bdd/stuff/client"
	"gitlab.com/snarksliveshere/company-api/tests/bdd/stuff/config"
)

var test *bddTest

type bddTest struct {
	clients         client.HTTPClient
	db              *pgxpool.Pool
	code            int
	resp            []byte
	env             config.Env
	request         *http.Request
	responseCompany company.Protocol
	requestCompany  company.Protocol
	ids             []string
	token           string
	kafkaEvents     []interface{}
	eventHandler    consumer.EventHandler
}

func TestMain(m *testing.M) {
	cfg := config.GetConfigs()

	status := godog.TestSuite{
		Name:                 "bdd",
		TestSuiteInitializer: InitializeTestSuite,
		ScenarioInitializer:  InitializeScenario,
		Options: &godog.Options{
			Format:        "pretty",
			Paths:         []string{cfg.TestPath},
			Randomize:     0,
			StopOnFailure: false,
		},
	}.Run()

	if st := m.Run(); st > status {
		status = st
	}
	os.Exit(status)
}

func InitializeTestSuite(ctx *godog.TestSuiteContext) {
	test = new(bddTest)
	ctx.BeforeSuite(test.start)
	ctx.AfterSuite(test.end)
}

func InitializeScenario(ctx *godog.ScenarioContext) {
	commonTest(ctx)
	handleCompanyProcess(ctx)
	KafkaTest(ctx)
}

func (test *bddTest) start() {
	fmt.Println("**************START....................")
	test.env = config.GetConfigs()
	test.clients = client.NewHTTPClient(test.env.TestHostTimeout)
	err := test.createJWT()
	if err != nil {
		log.Fatal(err)
	}
	db, err := bootstrap.SetupConnectionPool(context.Background(), test.env.PG)
	if err != nil {
		log.Fatal(err)
	}

	test.db = db

	fmt.Println("..............START******************")
}

func (test *bddTest) end() {
	fmt.Println("**************END................")
	fmt.Println("finished")
}
