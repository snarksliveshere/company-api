//go:build bdd_tests

package bdd_tests

import (
	"fmt"
	"net/http"
	"net/url"

	"github.com/cucumber/godog"
	"github.com/cucumber/messages-go/v10"
)

const (
	urlCreate = "/company"
	urlUUID   = "/company/%s"
)

var headers = map[string]string{
	"Content-Type": "application/json; charset=utf-8",
}

func handleCompanyProcess(ctx *godog.ScenarioContext) {
	ctx.Step(`^Create Company$`, test.createCompany)
	ctx.Step(`^Get Company$`, test.getCompany)
	ctx.Step(`^Patch Company$`, test.patchCompany)
	ctx.Step(`^Delete Companies$`, test.deleteCompanies)
	ctx.Step(`^Delete Company$`, test.deleteCompany)
}

func (test *bddTest) patchCompany(args *messages.PickleStepArgument_PickleDocString) error {
	b := []byte(args.GetContent())

	path := FormURL("http", test.env.TestHostHTTP, fmt.Sprintf(urlUUID, test.responseCompany.ID))
	req, err := test.clients.CreateHTTPRequest(http.MethodPatch, path, b, nil)
	if err != nil {
		return err
	}
	test.request = req
	return nil
}

func (test *bddTest) getCompany() error {
	path := FormURL("http", test.env.TestHostHTTP, fmt.Sprintf(urlUUID, test.responseCompany.ID))
	req, err := test.clients.CreateHTTPRequest(http.MethodGet, path, nil, nil)
	if err != nil {
		return err
	}
	test.request = req
	return nil
}

func (test *bddTest) createCompany(args *messages.PickleStepArgument_PickleDocString) error {
	b := []byte(args.GetContent())

	path := FormURL("http", test.env.TestHostHTTP, urlCreate)
	req, err := test.clients.CreateHTTPRequest(http.MethodPost, path, b, headers)
	if err != nil {
		return err
	}
	test.request = req

	return nil
}

func (test *bddTest) deleteCompanies() error {
	for _, v := range test.ids {
		err := test.delete(v)
		if err != nil {
			return err
		}
		err = test.callRequest()
		if err != nil {
			return err
		}
	}
	test.ids = nil
	return nil
}

func (test *bddTest) deleteCompany() error {
	return test.delete(test.responseCompany.ID)
}

func (test *bddTest) delete(uuid string) error {
	path := FormURL("http", test.env.TestHostHTTP, fmt.Sprintf(urlUUID, uuid))
	req, err := test.clients.CreateHTTPRequest(http.MethodDelete, path, nil, nil)
	if err != nil {
		return err
	}
	test.request = req
	return nil
}

func FormURL(scheme, host, path string) string {
	u := url.URL{
		Scheme: scheme,
		Host:   host,
		Path:   path,
	}
	return u.String()
}
