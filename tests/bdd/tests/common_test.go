//go:build bdd_tests

package bdd_tests

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/cucumber/godog"
	"github.com/cucumber/messages-go/v10"
	"github.com/fatih/structs"
	"github.com/georgysavva/scany/v2/pgxscan"
	"github.com/gofiber/fiber/v2"

	"gitlab.com/snarksliveshere/company-api/internal/app/api/v1/company"
	"gitlab.com/snarksliveshere/company-api/pkg/auth"
	"gitlab.com/snarksliveshere/company-api/pkg/server"
)

const (
	entityKafka = "kafka"
)

var credAuth = server.User{
	Name:          "M",
	Surname:       "D",
	Email:         "e@e.com",
	Authenticated: true,
}

func commonTest(ctx *godog.ScenarioContext) {
	ctx.Step(`^Breakpoint$`, test.breakpoint)
	ctx.Step(`^Wait For (\d+) MilliSeconds$`, test.waitForMilliSeconds)
	ctx.Step(`^Call Request$`, test.callRequest)
	ctx.Step(`^Response Code Must Be (\d+)$`, test.responseCodeMustBe)
	ctx.Step(`^Fields Must Be Equal To$`, test.fieldsMustBeEqualTo)
	ctx.Step(`^Last Field "([^"]*)" In EventLogTable Must Be "([^"]*)"$`,
		test.lastFieldInEventLogTableMustBe)
	ctx.Step(`^Clear "([^"]*)"$`, test.clear)
}

func (test *bddTest) clear(entity string) error {
	switch entity {
	case entityKafka:
		test.kafkaEvents = nil
	}
	return nil
}

func (test *bddTest) lastFieldInEventLogTableMustBe(field, value string) error {
	qu, args, err := sq.Select(field).
		From("event_log").
		Where(sq.Eq{
			"entity_id": test.responseCompany.ID,
		}).
		OrderBy("created_at DESC").
		Limit(1).
		PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return err
	}
	var val string
	err = pgxscan.Get(context.Background(), test.db, &val, qu, args...)
	if err != nil {
		return err
	}
	if value != val {
		return fmt.Errorf("value must be:%s, instead:%s", value, val)
	}
	return nil
}

func (test *bddTest) createJWT() error {
	u := &credAuth
	claim := server.UserClaim{User: u}
	token, err := auth.CreateJWT(claim, test.env.Secret.EncKey)
	if err != nil {
		return err
	}
	test.token = token
	return nil
}

func (test *bddTest) fieldsMustBeEqualTo(args *messages.PickleStepArgument_PickleDocString) error {
	var m map[string]interface{}
	err := json.Unmarshal([]byte(args.GetContent()), &m)
	if err != nil {
		return fmt.Errorf(err.Error())
	}

	resp := company.ConvertProtocolToModel(test.responseCompany)

	for k, v := range m {
		err := FieldInStructMustBeEqualTo(k, fmt.Sprintf("%v", v), resp)
		if err != nil {
			return fmt.Errorf(err.Error())
		}
	}
	return nil
}

func (test *bddTest) responseCodeMustBe(code int) error {
	if test.code != code {
		return fmt.Errorf("code must be:%d, %d instead", code, test.code)
	}

	return nil
}

func (test *bddTest) callRequest() error {
	test.request.Header.Set(server.HeaderXToken, test.token)
	b, code, err := test.clients.DoRequest(test.request)
	if code >= http.StatusBadRequest {
		test.code = code
		test.resp = b
		return nil
	}
	if err != nil {
		return fmt.Errorf("can't do request, because:%v, body:%s", err, string(b))
	}
	test.resp = b
	test.code = code

	if code == fiber.StatusNoContent {
		return nil
	}

	var res company.Protocol
	err = json.Unmarshal(b, &res)
	if err != nil {
		return err
	}
	test.responseCompany = res
	test.ids = append(test.ids, res.ID)
	return nil
}

func (test *bddTest) breakpoint() error {
	return nil
}

func (test *bddTest) waitForMilliSeconds(numMilliSeconds int) error {
	time.Sleep(time.Duration(numMilliSeconds) * time.Millisecond)
	return nil
}

func FieldInStructMustBeEqualTo(field, equalTo string, entity interface{}) error {
	val, err := GetValueFromStruct(field, entity)
	if err != nil {
		return err
	}

	if fmt.Sprintf("%v", val) != equalTo {
		return fmt.Errorf("must be equal to:[%s], instead:%v", equalTo, val)
	}
	return nil
}

func GetValueFromStruct(field string, entity interface{}) (interface{}, error) {
	var m map[string]interface{}
	if structs.IsStruct(entity) {
		r := structs.New(entity)
		m = r.Map()
	} else {
		val, ok := entity.(map[string]interface{})
		if !ok {
			return nil, fmt.Errorf("not a struct and not a map")
		}
		m = val
	}

	sl := strings.Split(field, ".")
	if len(sl) == 1 {
		_, ok := m[field]
		if !ok {
			return nil, fmt.Errorf("there is no such field:[%s] in this struct:[%v]", field, entity)
		}
		if m[field] == nil {
			return nil, fmt.Errorf("field must not be empty")
		}
	}

	for _, v := range sl {
		_, ok := m[v]
		if !ok {
			return nil, fmt.Errorf("there is no such field:[%s] in this struct:[%v]", field, entity)
		}
		val, ok := m[v].(map[string]interface{})
		if !ok {
			break
		}
		m = val
	}

	searchedField := sl[len(sl)-1]
	return m[searchedField], nil
}
