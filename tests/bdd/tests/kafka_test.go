//go:build bdd_tests

package bdd_tests

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/Shopify/sarama"
	"github.com/cucumber/godog"

	"gitlab.com/snarksliveshere/kafka-batch-lib/consumer"

	"gitlab.com/snarksliveshere/company-api/internal/model"
)

type handler struct {
	test *bddTest
}

func NewKafkaConsumer(test *bddTest) *handler {
	return &handler{
		test: test,
	}
}

func KafkaTest(ctx *godog.ScenarioContext) {
	ctx.Step(`^Consume Events$`, test.consumeEvents)
	ctx.Step(`^Add Event Handler$`, test.addEventHandler)
	ctx.Step(`^Event EntityID Must Be Equal And Action "([^"]*)"$`,
		test.eventEntityIDMustBeEqualAndAction)
	ctx.Step(`^Event Length Must Be (\d+)$`, test.eventLengthMustBe)

}

func (test *bddTest) eventEntityIDMustBeEqualAndAction(action string) error {
	actions := strings.Split(action, ",")

	uuid := test.responseCompany.ID

	for _, v := range test.kafkaEvents {
		ev, ok := v.(*model.Event)
		if !ok {
			return fmt.Errorf("event must cast to event")
		}
		if uuid != ev.EntityID {
			return fmt.Errorf("uuud:%s and antityID:%s must be equal", uuid, ev.EntityID)
		}
		if !isValInSlice(ev.Action, actions) {
			return fmt.Errorf("action:%s must be in slice", ev.Action)
		}
	}
	return nil
}

func isValInSlice(val string, sl []string) bool {
	for _, v := range sl {
		if v == val {
			return true
		}
	}
	return false
}

func (test *bddTest) eventLengthMustBe(ln int) error {
	if ln != len(test.kafkaEvents) {
		return fmt.Errorf("len of events must be:%d instead:%d", ln, len(test.kafkaEvents))
	}
	return nil
}

func (test *bddTest) consumeEvents() error {
	return test.listenQueueNewest()
}

func (test *bddTest) addEventHandler() error {
	test.eventHandler = NewKafkaConsumer(test)
	return nil
}

func (h *handler) ExtractEventFromMsg(_ context.Context, topic string, msg []byte) (string, interface{}, error) {
	var event model.Event
	err := json.Unmarshal(msg, &event)
	return topic, &event, err
}

func (h *handler) ProcessEvents(_ context.Context, events map[string][]interface{}) map[string]error {
	errorMap := make(map[string]error)
	for _, batch := range events {
		h.test.kafkaEvents = append(h.test.kafkaEvents, batch...)
	}
	return errorMap
}

// TODO: This is rather code for tests, not for an application
func (test *bddTest) listenQueueNewest() error {
	options := getKafkaOptions("cgroup")
	client := MustInitKafkaClient(test.env.Kafka.Brokers, options)
	ctx, _ := context.WithCancel(context.Background())
	ch := make(chan error, 1)
	go func(ch chan<- error) {
		defer func() { ch = nil }()
		c, err := consumer.NewConsumer(
			client,
			[]string{test.env.Producer.EventTopic},
			func() time.Duration {
				return test.env.Kafka.BatchDuration
			},
			func() int {
				return test.env.Kafka.BatchSize
			},
			func() time.Duration {
				return 0
			},
			test.eventHandler,
		)
		if err != nil {
			err = fmt.Errorf("failed to create consumer:%w", err)
			ch <- err
		}

		err = c.Run(ctx)
		if err != nil {
			err = fmt.Errorf("failed to run consumer:%w", err)
			return
		}
		for {
			select {
			case <-ctx.Done():
				return
			}
		}
	}(ch)

	ch = nil
	return nil
}

func getKafkaOptions(clientID string) *sarama.Config {
	options := sarama.NewConfig()
	options.Version = sarama.V2_1_1_0
	options.ClientID = clientID
	return options
}

func MustInitKafkaClient(brokers []string, options *sarama.Config) sarama.Client {
	client, err := sarama.NewClient(brokers, options)
	if err != nil {
		log.Fatal("failed to init kafka client")
	}
	return client
}
