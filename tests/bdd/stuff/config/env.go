//go:build bdd_tests

package config

import (
	"log"
	"time"

	"github.com/kelseyhightower/envconfig"

	"gitlab.com/snarksliveshere/company-api/internal/config"
)

const envPrefix = ""

type Env struct {
	TestHostHTTP    string        `envconfig:"TEST_HOST_HTTP" default:"localhost:8080"`
	TestHostTimeout time.Duration `envconfig:"TEST_HOST_TIMEOUT" default:"10s"`
	DefaultSchema   string        `envconfig:"DEFAULT_SCHEMA" default:"http"`
	TestPath        string        `envconfig:"TEST_PATH" default:"../features"`

	Kafka    *Kafka
	PG       *config.PG
	Secret   *config.Secret
	Producer *config.Producer
}

func GetConfigs() Env {
	var cfg Env
	err := envconfig.Process(envPrefix, &cfg)
	if err != nil {
		log.Fatalf("cant get config, err: [%s]", err.Error())
	}
	return cfg
}

type Kafka struct {
	*config.Kafka
	BatchSize     int           `split_words:"true" default:"10"`
	BatchDuration time.Duration `split_words:"true" default:"1ms"`
}
