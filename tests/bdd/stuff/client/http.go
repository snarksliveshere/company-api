package client

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/rs/zerolog/log"
)

const (
	defaultTimeout = 5 * time.Second
)

type httpclient struct {
	timeout time.Duration
}

// HTTPClient access to HTTPClient
type HTTPClient interface {
	CreateHTTPRequest(method, url string, body []byte, headers map[string]string) (*http.Request, error)
	DoRequest(req *http.Request) ([]byte, int, error)
}

// NewHTTPClient return HTTPClient
func NewHTTPClient(timeout time.Duration) *httpclient {
	if timeout == 0 {
		timeout = defaultTimeout
	}

	return &httpclient{
		timeout: timeout,
	}
}

func (hc *httpclient) CreateHTTPRequest(method, url string, body []byte, headers map[string]string) (*http.Request, error) {
	req, err := http.NewRequest(method, url, bytes.NewReader(body))
	if err != nil {
		return nil, err
	}
	if len(headers) > 0 {
		for k, v := range headers {
			req.Header.Set(k, v)
		}
	}

	return req, nil
}

func (hc *httpclient) DoRequest(req *http.Request) ([]byte, int, error) {
	client := http.Client{
		Timeout: hc.timeout,
	}
	resp, err := client.Do(req)
	if resp != nil {
		defer func() {
			if err := resp.Body.Close(); err != nil {
				log.Error().Err(err).Msg("can't close body")
			}
		}()
	}
	if err != nil {
		return nil, 0, fmt.Errorf("can't do request, because:%v", err)
	}

	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, 0, err
	}

	if resp.StatusCode >= http.StatusBadRequest {
		return nil, resp.StatusCode, fmt.Errorf("%s", string(bodyBytes))
	}

	return bodyBytes, resp.StatusCode, nil
}
