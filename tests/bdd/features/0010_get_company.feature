Feature: Test Get Company

  Scenario: Success Get Company
    Given Wait For 500 MilliSeconds
    And Clear "kafka"
    Given Create Company
      """
      {
          "name": "companyNameGet",
          "description": "companyDescription",
          "amountEmployees": 1,
          "registered": true,
          "type": "NonProfit"
      }
      """

    When Call Request
    And Response Code Must Be 201

    Given Get Company
    When Call Request
    And Response Code Must Be 200

    Given Fields Must Be Equal To
    """
      {
          "Name": "companyNameGet",
          "Description": "companyDescription",
          "AmountEmployees": 1,
          "Registered": true,
          "CompanyType": "NonProfit"
      }
    """
    Given Delete Company
    When Call Request

    And Response Code Must Be 204

    Given Wait For 500 MilliSeconds
    Given Event Length Must Be 2
    Given Event EntityID Must Be Equal And Action "create,delete"
    Given Clear "kafka"

