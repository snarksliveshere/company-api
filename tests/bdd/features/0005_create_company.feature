Feature: Test Create And Delete Company

  Scenario: Start Kafka
    Given Add Event Handler
    Given Clear "kafka"
    Given Consume Events
    # I need to give time to start
    Given Wait For 5000 MilliSeconds

  Scenario: Success Create Company
    Given Create Company
      """
      {
          "name": "companyName",
          "description": "companyDescription",
          "amountEmployees": 1,
          "registered": true,
          "type": "NonProfit"
      }
      """

    When Call Request
    And Response Code Must Be 201

    Given Last Field "action" In EventLogTable Must Be "create"

    Given Delete Company
    When Call Request

    And Response Code Must Be 204

    Given Last Field "action" In EventLogTable Must Be "delete"

    Given Wait For 500 MilliSeconds

    Given Event Length Must Be 2
    Given Event EntityID Must Be Equal And Action "create,delete"

    And Clear "kafka"

  Scenario: Err Create Company Bad Request
    Given Create Company
      """
      {
          "name": "",
          "description": "companyDescription",
          "amountEmployees": 1,
          "registered": true,
          "type": "NonProfit"
      }
      """

    When Call Request
    And Response Code Must Be 400

    Given Create Company
      """
      {
          "name": "companyNameTooLongName",
          "description": "companyDescription",
          "amountEmployees": 1,
          "registered": true,
          "type": "NonProfit"
      }
      """

    When Call Request
    And Response Code Must Be 400

    Given Create Company
      """
      {
          "description": "companyDescription",
          "amountEmployees": 1,
          "registered": true,
          "type": "NonProfit"
      }
      """

    When Call Request
    And Response Code Must Be 400

    Given Create Company
      """
      {
          "name": "someName",
          "description": "companyDescription",
          "registered": true,
          "type": "NonProfit"
      }
      """

    When Call Request
    And Response Code Must Be 400

    Given Create Company
      """
      {
          "name": "someName",
          "description": "companyDescription",
          "amountEmployees": 1,
          "type": "NonProfit"
      }
      """

    When Call Request
    And Response Code Must Be 400

    Given Create Company
      """
      {
          "name": "someName",
          "description": "companyDescription",
          "amountEmployees": 1,
          "registered": true,
          "type": "NonProfit1"
      }
      """

    When Call Request
    And Response Code Must Be 400

    Given Create Company
      """
      {
          "name": "someName",
          "description": "companyDescription",
          "amountEmployees": 1,
          "registered": true
      }
      """

    When Call Request
    And Response Code Must Be 400

  Scenario: DB Err Create Company Unique
    Given Create Company
      """
      {
          "name": "companyNameNew",
          "description": "companyDescription",
          "amountEmployees": 1,
          "registered": true,
          "type": "NonProfit"
      }
      """

    When Call Request
    And Response Code Must Be 201

    Given Create Company
      """
      {
          "name": "companyNameNew",
          "description": "companyDescription",
          "amountEmployees": 1,
          "registered": true,
          "type": "NonProfit"
      }
      """

    When Call Request
    And Response Code Must Be 400

    Given Delete Companies

