Feature: Test Patch Company

  Scenario: Success Patch Company
    Given Wait For 500 MilliSeconds
    And Clear "kafka"
    Given Create Company
      """
      {
          "name": "namePatch",
          "description": "companyDescription",
          "amountEmployees": 1,
          "registered": true,
          "type": "NonProfit"
      }
      """

    When Call Request
    And Response Code Must Be 201

    Given Last Field "action" In EventLogTable Must Be "create"

    Given Patch Company
       """
      {
          "description": "companyDescription1"
      }
      """

    When Call Request
    And Response Code Must Be 200

    Given Last Field "action" In EventLogTable Must Be "patch"

    Given Fields Must Be Equal To
    """
      {
        "Description": "companyDescription1"
      }
    """

    Given Patch Company
       """
      {
           "amountEmployees": 10
      }
      """

    When Call Request
    And Response Code Must Be 200

    Given Fields Must Be Equal To
    """
      {
           "AmountEmployees": 10
      }
    """

    Given Patch Company
       """
      {
          "registered": false
      }
      """

    When Call Request
    And Response Code Must Be 200

    Given Fields Must Be Equal To
    """
      {
          "Registered": false
      }
    """

    Given Patch Company
       """
      {
         "name": "namePatchMod"
      }
      """

    When Call Request
    And Response Code Must Be 200

    Given Fields Must Be Equal To
    """
      {
          "Name": "namePatchMod"
      }
    """

    Given Patch Company
       """
      {
          "type": "Cooperative"
      }
      """

    When Call Request
    And Response Code Must Be 200

    Given Fields Must Be Equal To
    """
      {
          "CompanyType": "Cooperative"
      }
    """

    Given Fields Must Be Equal To
    """
      {
          "Name": "namePatchMod",
          "Description": "companyDescription1",
          "AmountEmployees": 10,
          "Registered": false,
          "CompanyType": "Cooperative"
      }
    """
    Given Delete Company
    When Call Request
    And Response Code Must Be 204

    Given Wait For 500 MilliSeconds
    Given Event Length Must Be 7
    Given Event EntityID Must Be Equal And Action "create,patch,delete"

