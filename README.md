# company-api

### Description

I decided not to couple sending events and processing companies.  
Therefore, There are two parts in this application.  
First part (`cmd/company-api`) is responsible for storing companies and events in database.  
Event is some kind of entity log. Sometimes It can be very convenient.    

The second part (`cmd/producer`) is responsible for producing events to Kafka. Its kind of cron.    
After the event is sent, it is given the status produced:true

#### CI
I wrote BDD integration tests for API, consuming and events and added them to CI.  
There is also a linter and autotests.  
The images for company-api and producer are built by tag and pushed in registry.  
They are called by pattern `registry.gitlab.com/snarksliveshere/company-api/{app_name}:v0.0.5`    
The entire pipeline can be viewed here
```https://gitlab.com/snarksliveshere/company-api/-/pipelines/845975714```

#### ...and the rest

There is also a docker-compose file.  
If containers are running, you can send requests from the `examples` folder  

I also added a few routine to migration. I've always found them to be very convenient.    
I put in `/pkg` those packages that would be nice to put in the library

Something I had time to do, something I wanted to do, but didn't have time to do. I hope I didn't forget anything important.  

Have a nice day!


