package main

import (
	"context"
	"log"
	"os"

	"github.com/rs/zerolog"

	"gitlab.com/snarksliveshere/company-api/internal/app/api/v1/router"
	"gitlab.com/snarksliveshere/company-api/internal/config"
	"gitlab.com/snarksliveshere/company-api/pkg/server"
)

func main() {
	ctx := context.Background()
	cfg := config.Init()
	logLevel, err := zerolog.ParseLevel(cfg.LogLevel)
	if err != nil {
		log.Fatalf("unable to parse log level: %v", err)
	}
	logger := zerolog.New(os.Stdout).Level(logLevel).With().Timestamp().Logger()
	app := server.NewServer(logger, config.ConvertToServerCfg(cfg.Server.HTTP))
	router.InitRouter(ctx, logger, cfg, app)
	app.Run()
}
