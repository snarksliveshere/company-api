package main

import (
	"log"
	"os"

	"github.com/rs/zerolog"
	"golang.org/x/net/context"

	"gitlab.com/snarksliveshere/company-api/internal/bootstrap"
	"gitlab.com/snarksliveshere/company-api/internal/config"
)

func main() {
	ctx := context.Background()
	cfg := config.InitProducerConfig()
	logLevel, err := zerolog.ParseLevel(cfg.LogLevel)
	if err != nil {
		log.Fatalf("unable to parse log level: %v", err)
	}
	logger := zerolog.New(os.Stdout).Level(logLevel).With().Timestamp().Logger()

	impl, err := bootstrap.InitializeProducer(ctx, logger, cfg)
	if err != nil {
		logger.Fatal().Msgf("unable to init producer service, because:%v", err)
	}
	logger.Info().Msg("Start Producer")
	impl.StartProduceEvents(ctx, cfg.Producer)
}
