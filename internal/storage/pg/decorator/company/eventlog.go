package company

import (
	"context"
	"encoding/json"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"

	"gitlab.com/snarksliveshere/company-api/internal/model"
	"gitlab.com/snarksliveshere/company-api/internal/model/interfaces"
	"gitlab.com/snarksliveshere/company-api/internal/storage/pg"
	"gitlab.com/snarksliveshere/company-api/pkg/eventlog"
	pg2 "gitlab.com/snarksliveshere/company-api/pkg/pg"
)

const (
	entityCompany = "company"

	actionCreate = "create"
	actionPatch  = "patch"
	actionDelete = "delete"
)

type eventLogDecorator struct {
	db *pgxpool.Pool
}

// NewEventLogDecorator Creating a decorator for a company operation
// TODO: I decided not to couple sending events and processing companies.
// To do this, I created a separate table to store events for later processing.
// Storing events is not associated with the operations of creation, modification, deletion, but must be in the transaction.
func NewEventLogDecorator(db *pgxpool.Pool) *eventLogDecorator {
	return &eventLogDecorator{
		db: db,
	}
}

func (e eventLogDecorator) CreateCompany(ctx context.Context, company model.Company) error {
	return pg2.TransactionWrapper(ctx, e.db, func(ctx context.Context, tx pgx.Tx) error {
		store := e.newServiceCompanyStore(tx)
		err := store.CreateCompany(ctx, company)
		if err != nil {
			return err
		}

		record := &eventlog.Record{
			Action:     actionCreate,
			EntityName: entityCompany,
			EntityID:   company.ID,
			Data:       prepareJSON(company),
		}

		quRecord, args, err := eventlog.GetSaveEventLogQuery(record).ToSql()
		if err != nil {
			return err
		}
		_, err = tx.Exec(ctx, quRecord, args...)

		return err
	})
}

func (e eventLogDecorator) PatchCompany(ctx context.Context, company model.Company) error {
	return pg2.TransactionWrapper(ctx, e.db, func(ctx context.Context, tx pgx.Tx) error {
		store := e.newServiceCompanyStore(tx)
		err := store.PatchCompany(ctx, company)
		if err != nil {
			return err
		}

		record := &eventlog.Record{
			Action:     actionPatch,
			EntityName: entityCompany,
			EntityID:   company.ID,
			Data:       prepareJSON(company),
		}
		quRecord, args, err := eventlog.GetSaveEventLogQuery(record).ToSql()
		if err != nil {
			return err
		}
		_, err = tx.Exec(ctx, quRecord, args...)

		return err
	})
}

func (e eventLogDecorator) DeleteCompany(ctx context.Context, uuid string) error {
	return pg2.TransactionWrapper(ctx, e.db, func(ctx context.Context, tx pgx.Tx) error {
		store := e.newServiceCompanyStore(tx)
		err := store.DeleteCompany(ctx, uuid)
		if err != nil {
			return err
		}
		record := &eventlog.Record{
			Action:     actionDelete,
			EntityName: entityCompany,
			EntityID:   uuid,
			Data:       "",
		}
		quRecord, args, err := eventlog.GetSaveEventLogQuery(record).ToSql()
		if err != nil {
			return err
		}
		_, err = tx.Exec(ctx, quRecord, args...)

		return err
	})
}

func (e eventLogDecorator) GetCompany(ctx context.Context, uuid string) (model.Company, error) {
	store := e.newServiceCompanyStore(e.db)
	return store.GetCompany(ctx, uuid)
}

func prepareJSON(data interface{}) string {
	if marshalled, err := json.Marshal(data); err == nil {
		return string(marshalled)
	}
	return ""
}

func (e eventLogDecorator) newServiceCompanyStore(db pg2.DBConn) interfaces.ServiceCompany {
	return pg.NewStore(db)
}
