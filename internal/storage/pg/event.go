package pg

import (
	"context"

	sq "github.com/Masterminds/squirrel"
	"github.com/georgysavva/scany/v2/pgxscan"

	"gitlab.com/snarksliveshere/company-api/internal/model"
)

// TODO: For now, I assume that events will be handled by a single instance

const (
	tblEventLog = "event_log"

	fieldAction     = "action"
	fieldEntityID   = "entity_id"
	fieldEntityName = "entity_name"
	fieldData       = "data"
	fieldProduced   = "produced"
)

// GetUnsentEvents receives model.Events with produced status false with specified limit
func (s *store) GetUnsentEvents(ctx context.Context, limit uint64) ([]model.Event, error) {
	qu, args, err := sq.Select(fieldID, fieldAction, fieldEntityID, fieldEntityName, fieldData).
		From(tblEventLog).
		Where(sq.Eq{fieldProduced: false}).
		Limit(limit).
		PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return nil, err
	}
	var events []model.Event
	err = pgxscan.Select(ctx, s.db, &events, qu, args...)
	return events, err
}

// SetEventsProduced updates event_log table, sets produced status to true
func (s *store) SetEventsProduced(ctx context.Context, ids []int) error {
	qu, args, err := sq.Update(tblEventLog).
		Set(fieldProduced, true).
		Where(sq.Eq{fieldID: ids}).
		PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return err
	}
	_, err = s.db.Exec(ctx, qu, args...)
	return err
}
