package pg

import (
	"gitlab.com/snarksliveshere/company-api/pkg/pg"
)

type store struct {
	db pg.DBConn
}

// NewStore sets up a new database store
func NewStore(db pg.DBConn) *store {
	return &store{
		db: db,
	}
}
