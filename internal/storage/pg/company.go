package pg

import (
	"context"
	"fmt"

	sq "github.com/Masterminds/squirrel"

	"github.com/georgysavva/scany/v2/pgxscan"

	"gitlab.com/snarksliveshere/company-api/internal/model"
)

const (
	tblCompany = "company"

	fieldID              = "id"
	fieldName            = "name"
	fieldDescription     = "description"
	fieldAmountEmployees = "amount_employees"
	fieldRegistered      = "registered"
	fieldType            = "type"
)

var companyColumns = []string{
	fieldID,
	fieldName,
	fieldDescription,
	fieldAmountEmployees,
	fieldRegistered,
	fieldType,
}

// DeleteCompany delete company by uuid
func (s *store) DeleteCompany(ctx context.Context, uuid string) error {
	qu, args, err := sq.Delete(tblCompany).
		Where(sq.Eq{fieldID: uuid}).
		PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return err
	}
	_, err = s.db.Exec(ctx, qu, args...)

	return err
}

// CreateCompany creates a company in the database with the provided parameters
// and returns the whole company data
func (s *store) CreateCompany(ctx context.Context, c model.Company) error {
	qu, args, err := sq.Insert(tblCompany).
		Columns(companyColumns...).
		Values(c.ID, c.Name, c.Description, c.AmountEmployees, c.Registered, c.CompanyType).
		PlaceholderFormat(sq.Dollar).ToSql()

	if err != nil {
		return fmt.Errorf("can't create checkout query because:%v", err)
	}
	_, err = s.db.Exec(ctx, qu, args...)
	return err
}

// PatchCompany creates a company in the database with the provided parameters
// and returns the whole company data
func (s *store) PatchCompany(ctx context.Context, c model.Company) error {
	qu, args, err := sq.Update(tblCompany).
		Where(sq.Eq{fieldID: c.ID}).
		Set(fieldName, c.Name).
		Set(fieldDescription, c.Description).
		Set(fieldAmountEmployees, c.AmountEmployees).
		Set(fieldRegistered, c.Registered).
		Set(fieldType, c.CompanyType).
		PlaceholderFormat(sq.Dollar).ToSql()

	if err != nil {
		return fmt.Errorf("can't create checkout query because:%v", err)
	}
	_, err = s.db.Exec(ctx, qu, args...)
	return err
}

// GetCompany returns company by uuid
func (s *store) GetCompany(ctx context.Context, uuid string) (model.Company, error) {
	qu, args, err := sq.Select(companyColumns...).
		From(tblCompany).
		Where(sq.Eq{fieldID: uuid}).
		PlaceholderFormat(sq.Dollar).ToSql()
	if err != nil {
		return model.Company{}, fmt.Errorf("can't create checkout query because:%v", err)
	}

	res := model.Company{}
	err = pgxscan.Get(ctx, s.db, &res, qu, args...)
	return res, err
}
