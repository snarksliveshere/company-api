package config

import (
	"fmt"
	"time"

	"github.com/kelseyhightower/envconfig"

	"gitlab.com/snarksliveshere/company-api/pkg/server"
)

type (
	Config struct {
		Server   *Server
		PG       *PG
		Secret   *Secret
		LogLevel string `split_words:"true" default:"info"`
	}

	ProducerConfig struct {
		PG       *PG
		Kafka    *Kafka
		Producer *Producer
		LogLevel string `split_words:"true" default:"info"`
	}

	Server struct {
		HTTP *HTTP
	}

	HTTP struct {
		Port         int           `required:"true"`
		ReadTimeout  time.Duration `split_words:"true" default:"5s" `
		WriteTimeout time.Duration `split_words:"true" default:"5s" `
		Delay        time.Duration `default:"1s"`
	}

	PG struct {
		Host     string `required:"true"`
		Port     uint16 `required:"true"`
		User     string
		Password string
		Database string        `required:"true"`
		MaxConns int32         `split_words:"true" default:"10"`
		MinConns int32         `split_words:"true" default:"2"`
		Timeout  time.Duration `default:"5s"`
	}

	Kafka struct {
		ClientID   string `split_words:"true" default:"kafkaClientID"`
		Brokers    []string
		SASLEnable bool `split_words:"true"`
		Username   string
		Password   string
		MaxRetries int `split_words:"true"`
	}

	Secret struct {
		EncKey string `split_words:"true" required:"true"`
	}

	Producer struct {
		Delay      time.Duration `default:"1s"`
		EventLimit uint64        `split_words:"true" default:"10"`
		EventTopic string        `split_words:"true" default:"company"`
	}
)

func Init() *Config {
	cfg := Config{}
	err := envconfig.Process("", &cfg)
	if err != nil {
		panic(fmt.Errorf("error on loading config: %s", err))
	}
	return &cfg
}

func InitProducerConfig() *ProducerConfig {
	cfg := ProducerConfig{}
	err := envconfig.Process("", &cfg)
	if err != nil {
		panic(fmt.Errorf("error on loading config: %s", err))
	}
	return &cfg
}

func GetPGConfig(cfg *Config) *PG {
	return cfg.PG
}

func GetPGProducerConfig(cfg *ProducerConfig) *PG {
	return cfg.PG
}

func GetKafkaConfig(cfg *ProducerConfig) *Kafka {
	return cfg.Kafka
}

func ConvertToServerCfg(cfg *HTTP) *server.HTTPServer {
	return &server.HTTPServer{
		Port:          cfg.Port,
		ReadTimeout:   cfg.ReadTimeout,
		WriteTimeout:  cfg.WriteTimeout,
		ShutdownDelay: cfg.Delay,
	}
}
