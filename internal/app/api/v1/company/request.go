package company

import (
	"fmt"
	"regexp"
)

// TODO: should be in lib
const (
	UUIDRegexp     = `^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}$`
	UUIDNumSymbols = 36
)

const (
	ParamCompanyID = "companyID"

	TypeCorporations       = "Corporations"
	TypeNonProfit          = "NonProfit"
	TypeCooperative        = "Cooperative"
	TypeSoleProprietorship = "Proprietorship"
)

type (
	TypeOfCompany string

	Protocol struct {
		ID              string         `json:"id"`
		Name            *string        `json:"name"`
		Description     *string        `json:"description"`
		AmountEmployees *uint32        `json:"amountEmployees"`
		Registered      *bool          `json:"registered"`
		CompanyType     *TypeOfCompany `json:"type"`
	}
)

// Validate type of company
func (t TypeOfCompany) Validate() error {
	switch t {
	case TypeCorporations, TypeNonProfit, TypeCooperative, TypeSoleProprietorship:
		return nil
	default:
		return fmt.Errorf("unknown type of company")
	}
}

func validateCreateCompanyRequest(req Protocol) error {
	if req.Name == nil {
		return fmt.Errorf("field name in company request must exist")
	}
	if len([]rune(*req.Name)) > 15 || len(*req.Name) == 0 {
		return fmt.Errorf("inappropriate company name: %s", *req.Name)
	}
	if req.Description != nil && len([]rune(*req.Description)) > 3000 {
		return fmt.Errorf("too long company description: %s", *req.Description)
	}
	if req.AmountEmployees == nil {
		return fmt.Errorf("field amount employees in company request must exist")
	}
	if req.Registered == nil {
		return fmt.Errorf("field registered in company request must exist")
	}
	if req.CompanyType == nil {
		return fmt.Errorf("field company type in company request must exist")
	}
	if err := req.CompanyType.Validate(); err != nil {
		return err
	}
	return nil
}

// ValidateUUID validate uuid, should be in library
func ValidateUUID(uuid string) error {
	if len(uuid) != 36 {
		return fmt.Errorf("there must be %d characters in the uuid:%s, %d instead",
			UUIDNumSymbols, uuid, len(uuid))
	}
	m, err := regexp.MatchString(UUIDRegexp, uuid)
	if err != nil {
		return fmt.Errorf("error:%v when match uuid", err)
	}
	if !m {
		return fmt.Errorf("can't match uuid:%s to pattern", uuid)
	}
	return nil
}

func mergeCompanies(req, db Protocol) Protocol {
	merged := db
	if req.Name != nil {
		merged.Name = req.Name
	}
	if req.Description != nil {
		merged.Description = req.Description
	}
	if req.AmountEmployees != nil {
		merged.AmountEmployees = req.AmountEmployees
	}
	if req.Registered != nil {
		merged.Registered = req.Registered
	}
	if req.CompanyType != nil {
		merged.CompanyType = req.CompanyType
	}

	return merged
}
