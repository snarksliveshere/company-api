package company

import "gitlab.com/snarksliveshere/company-api/internal/model"

// ConvertProtocolToModel converting protocol to model. Also used in tests
func ConvertProtocolToModel(req Protocol) model.Company {
	return model.Company{
		ID:              req.ID,
		Name:            *req.Name,
		Description:     *req.Description,
		AmountEmployees: *req.AmountEmployees,
		Registered:      *req.Registered,
		CompanyType:     string(*req.CompanyType),
	}
}

// ConvertModelToProtocol converting model to protocol
func ConvertModelToProtocol(c model.Company) Protocol {
	return Protocol{
		ID:              c.ID,
		Name:            &c.Name,
		Description:     &c.Description,
		AmountEmployees: &c.AmountEmployees,
		Registered:      &c.Registered,
		CompanyType:     createTypeOfCompanyPointer(TypeOfCompany(c.CompanyType)),
	}
}

func createStringPointer(s string) *string {
	return &s
}

func createUIntPointer(n uint32) *uint32 {
	return &n
}

func createBoolPointer(t bool) *bool {
	return &t
}

func createTypeOfCompanyPointer(s TypeOfCompany) *TypeOfCompany {
	return &s
}
