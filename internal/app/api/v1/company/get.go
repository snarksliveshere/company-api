package company

import (
	"encoding/json"

	"github.com/gofiber/fiber/v2"

	"gitlab.com/snarksliveshere/company-api/pkg/errors"
)

// Get company
func (i *Implementation) Get(c *fiber.Ctx) error {
	uuid := c.Params(ParamCompanyID)
	err := ValidateUUID(uuid)
	if err != nil {
		i.log.Error().Err(err).Msg("can't validate uuid param when deleting a company")
		return c.Status(fiber.StatusBadRequest).JSON(errors.ConvertToJSONErr(err))
	}
	res, err := i.service.GetCompany(c.Context(), uuid)
	if err != nil {
		i.log.Error().Err(err).Msgf("can't get company by uuid:%s", uuid)
		if code, errPG := errors.GetStatusCodeByPGErr(err); errPG != nil {
			return c.Status(code).JSON(errors.ConvertToJSONErr(errPG))
		}
		return c.Status(fiber.StatusInternalServerError).JSON(errors.ConvertToJSONErr(err))
	}
	b, err := json.Marshal(ConvertModelToProtocol(res))
	if err != nil {
		i.log.Error().Err(err).Msg("can't marshal body when receiving a company")
		return c.Status(fiber.StatusNotAcceptable).JSON(errors.ConvertToJSONErr(err))
	}
	return c.Status(fiber.StatusOK).SendString(string(b))
}
