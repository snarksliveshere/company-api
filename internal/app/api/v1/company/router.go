package company

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
)

// Router returns company router
func Router(i *Implementation, app fiber.Router, authMiddleware fiber.Handler) fiber.Router {
	uuidGroup := fmt.Sprintf("/:%s<regex(%s)}>", ParamCompanyID, UUIDRegexp)
	app.Get(uuidGroup, i.Get)

	app.Use(authMiddleware)
	app.Post("/", i.Create)
	app.Patch(uuidGroup, i.Patch)
	app.Delete(uuidGroup, i.Delete)

	return app
}
