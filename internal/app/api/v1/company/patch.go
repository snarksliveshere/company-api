package company

import (
	"encoding/json"

	"github.com/gofiber/fiber/v2"

	"gitlab.com/snarksliveshere/company-api/pkg/errors"
)

// Patch company
func (i *Implementation) Patch(c *fiber.Ctx) error {
	uuid := c.Params(ParamCompanyID)
	err := ValidateUUID(uuid)
	if err != nil {
		i.log.Error().Err(err).Msg("can't validate uuid param when deleting a company")
		return c.Status(fiber.StatusBadRequest).JSON(errors.ConvertToJSONErr(err))
	}

	body := c.Body()
	req := Protocol{}
	err = json.Unmarshal(body, &req)
	if err != nil {
		i.log.Error().Err(err).Msg("can't unmarshal body when creating a company")
		return c.Status(fiber.StatusBadRequest).JSON(errors.ConvertToJSONErr(err))
	}

	dbCompany, err := i.service.GetCompany(c.Context(), uuid)
	if err != nil {
		i.log.Error().Err(err).Msg("can't get company")
		if code, errPG := errors.GetStatusCodeByPGErr(err); errPG != nil {
			return c.Status(code).JSON(errors.ConvertToJSONErr(errPG))
		}
		return c.Status(fiber.StatusInternalServerError).JSON(errors.ConvertToJSONErr(err))
	}

	merged := mergeCompanies(req, ConvertModelToProtocol(dbCompany))

	err = validateCreateCompanyRequest(merged)
	if err != nil {
		i.log.Error().Err(err).Msg("can't validate request when creating a company")
		return c.Status(fiber.StatusBadRequest).JSON(errors.ConvertToJSONErr(err))
	}

	err = i.service.PatchCompany(c.Context(), ConvertProtocolToModel(merged))
	if err != nil {
		i.log.Error().Err(err).Msg("can't create company")
		if code, errPG := errors.GetStatusCodeByPGErr(err); errPG != nil {
			return c.Status(code).JSON(errors.ConvertToJSONErr(errPG))
		}
		return c.Status(fiber.StatusInternalServerError).JSON(errors.ConvertToJSONErr(err))
	}
	b, err := json.Marshal(merged)
	if err != nil {
		i.log.Error().Err(err).Msg("can't marshal body when creating a company")
		return c.Status(fiber.StatusNotAcceptable).JSON(errors.ConvertToJSONErr(err))
	}

	return c.Status(fiber.StatusOK).SendString(string(b))
}
