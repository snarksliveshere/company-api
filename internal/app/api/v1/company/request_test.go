package company

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidateCreateCompanyRequest(t *testing.T) {
	t.Run("TestValidateCreateCompanyRequestError", func(t *testing.T) {
		casesError := []struct {
			in  Protocol
			out error
		}{
			{
				in: Protocol{
					Name:            nil,
					Description:     nil,
					AmountEmployees: nil,
					Registered:      nil,
					CompanyType:     nil,
				},
				out: fmt.Errorf("field name in company request must exist"),
			},
			{
				in: Protocol{
					Name:            createStringPointer("moreThan15Characters"),
					Description:     nil,
					AmountEmployees: nil,
					Registered:      nil,
					CompanyType:     nil,
				},
				out: fmt.Errorf("inappropriate company name: %s", "moreThan15Characters"),
			},
			{
				in: Protocol{
					Name:            createStringPointer(""),
					Description:     nil,
					AmountEmployees: nil,
					Registered:      nil,
					CompanyType:     nil,
				},
				out: fmt.Errorf("inappropriate company name: %s", ""),
			},
			{
				in: Protocol{
					Name:            createStringPointer("goodName"),
					Description:     nil,
					AmountEmployees: nil,
				},
				out: fmt.Errorf("field amount employees in company request must exist"),
			},
			{
				in: Protocol{
					Name:            createStringPointer("goodName"),
					Description:     nil,
					AmountEmployees: createUIntPointer(1),
					Registered:      nil,
				},
				out: fmt.Errorf("field registered in company request must exist"),
			},
			{
				in: Protocol{
					Name:            createStringPointer("goodName"),
					Description:     nil,
					AmountEmployees: createUIntPointer(1),
					Registered:      createBoolPointer(false),
					CompanyType:     nil,
				},
				out: fmt.Errorf("field company type in company request must exist"),
			},
			{
				in: Protocol{
					Name:            createStringPointer("goodName"),
					Description:     nil,
					AmountEmployees: createUIntPointer(1),
					Registered:      createBoolPointer(false),
					CompanyType:     createTypeOfCompanyPointer("wrongType"),
				},
				out: fmt.Errorf("unknown type of company"),
			},
		}
		for _, v := range casesError {
			t.Run("TestValidateCreateCompanyRequestError", func(t *testing.T) {
				err := validateCreateCompanyRequest(v.in)
				assert.EqualError(t, err, v.out.Error())
			})
		}
	})

	t.Run("TestValidateCreateCompanyRequestSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in Protocol
		}{
			{
				in: Protocol{
					Name:            createStringPointer("goodName"),
					Description:     nil,
					AmountEmployees: createUIntPointer(1),
					Registered:      createBoolPointer(false),
					CompanyType:     createTypeOfCompanyPointer(TypeCooperative),
				},
			},
			{
				in: Protocol{
					Name:            createStringPointer("goodName"),
					Description:     nil,
					AmountEmployees: createUIntPointer(0),
					Registered:      createBoolPointer(false),
					CompanyType:     createTypeOfCompanyPointer(TypeCorporations),
				},
			},
			{
				in: Protocol{
					Name:            createStringPointer("goodName"),
					Description:     createStringPointer("some normal description"),
					AmountEmployees: createUIntPointer(0),
					Registered:      createBoolPointer(false),
					CompanyType:     createTypeOfCompanyPointer(TypeNonProfit),
				},
			},
			{
				in: Protocol{
					Name:            createStringPointer("goodName"),
					Description:     createStringPointer("some normal description"),
					AmountEmployees: createUIntPointer(0),
					Registered:      createBoolPointer(false),
					CompanyType:     createTypeOfCompanyPointer(TypeSoleProprietorship),
				},
			},
		}
		for _, v := range casesSuccess {
			t.Run("TestValidateCreateCompanyRequestSuccess", func(t *testing.T) {
				err := validateCreateCompanyRequest(v.in)
				assert.NoError(t, err)
			})
		}
	})
}

func TestValidateUUID(t *testing.T) {
	t.Run("TestValidateUUIDError", func(t *testing.T) {
		casesError := []struct {
			in  string
			out string
		}{
			{
				in:  "some",
				out: "characters in the uuid",
			},
			{
				in:  "3eac267614cb6-4ce8-b571-aac1297caa2a",
				out: "can't match uuid:3eac267614cb6-4ce8-b571-aac1297caa2a to pattern",
			},
		}
		for _, v := range casesError {
			t.Run("TestValidateUUIDError", func(t *testing.T) {
				err := ValidateUUID(v.in)
				assert.ErrorContains(t, err, v.out)
			})
		}
	})
	t.Run("TestValidateUUIDSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in string
		}{
			{
				in: "3eac2676-4cb6-4ce8-b571-aac1297caa2a",
			},
			{
				in: "495e5cd3-be24-4085-8812-217aa6d49c0a",
			},
			{
				in: "00000000-0000-0000-0000-000000000000",
			},
		}
		for _, v := range casesSuccess {
			t.Run("TestValidateUUIDSuccess", func(t *testing.T) {
				err := ValidateUUID(v.in)
				assert.NoError(t, err)
			})
		}
	})
}
