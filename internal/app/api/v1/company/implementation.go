package company

import (
	"github.com/rs/zerolog"

	"gitlab.com/snarksliveshere/company-api/internal/model/interfaces"
)

// Implementation describe company implementation struct
type Implementation struct {
	log     zerolog.Logger
	service interfaces.ServiceCompany
}

// New returns the implementation of company handlers
func New(log zerolog.Logger, service interfaces.ServiceCompany) *Implementation {
	return &Implementation{
		log:     log,
		service: service,
	}
}
