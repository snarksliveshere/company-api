package company

import (
	"github.com/gofiber/fiber/v2"

	"gitlab.com/snarksliveshere/company-api/pkg/errors"
)

// Delete company
func (i *Implementation) Delete(c *fiber.Ctx) error {
	uuid := c.Params(ParamCompanyID)
	err := ValidateUUID(uuid)
	if err != nil {
		i.log.Error().Err(err).Msg("can't validate uuid param when deleting a company")
		return c.Status(fiber.StatusBadRequest).JSON(errors.ConvertToJSONErr(err))
	}
	err = i.service.DeleteCompany(c.Context(), uuid)
	if err != nil {
		i.log.Error().Err(err).Msgf("can't delete company by uuid:%s", uuid)
		if code, errPG := errors.GetStatusCodeByPGErr(err); errPG != nil {
			return c.Status(code).JSON(errors.ConvertToJSONErr(errPG))
		}
		return c.Status(fiber.StatusInternalServerError).JSON(errors.ConvertToJSONErr(err))
	}

	return c.SendStatus(fiber.StatusNoContent)
}
