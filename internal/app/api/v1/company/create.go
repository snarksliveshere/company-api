package company

import (
	"encoding/json"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"

	"gitlab.com/snarksliveshere/company-api/pkg/errors"
)

// Create company
func (i *Implementation) Create(c *fiber.Ctx) error {
	body := c.Body()
	req := Protocol{}
	err := json.Unmarshal(body, &req)
	if err != nil {
		i.log.Error().Err(err).Msg("can't unmarshal body when creating a company")
		return c.Status(fiber.StatusBadRequest).JSON(errors.ConvertToJSONErr(err))
	}
	err = validateCreateCompanyRequest(req)
	if err != nil {
		i.log.Error().Err(err).Msg("can't validate request when creating a company")
		return c.Status(fiber.StatusBadRequest).JSON(errors.ConvertToJSONErr(err))
	}
	req.ID = uuid.New().String()

	err = i.service.CreateCompany(c.Context(), ConvertProtocolToModel(req))
	if err != nil {
		i.log.Error().Err(err).Msg("can't create company")
		if code, errPG := errors.GetStatusCodeByPGErr(err); errPG != nil {
			return c.Status(code).JSON(errors.ConvertToJSONErr(errPG))
		}
		return c.Status(fiber.StatusInternalServerError).JSON(errors.ConvertToJSONErr(err))
	}
	b, err := json.Marshal(req)
	if err != nil {
		i.log.Error().Err(err).Msg("can't marshal body when creating a company")
		return c.Status(fiber.StatusNotAcceptable).JSON(errors.ConvertToJSONErr(err))
	}

	return c.Status(fiber.StatusCreated).SendString(string(b))
}
