package router

import (
	"context"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/fiber/v2/middleware/requestid"
	"github.com/rs/zerolog"

	"gitlab.com/snarksliveshere/company-api/internal/app/api/v1/company"
	"gitlab.com/snarksliveshere/company-api/internal/bootstrap"
	"gitlab.com/snarksliveshere/company-api/internal/config"
	"gitlab.com/snarksliveshere/company-api/pkg/server"
)

// InitRouter returns router for server
func InitRouter(ctx context.Context, log zerolog.Logger, cfg *config.Config, app *server.App) {
	companyImp, err := bootstrap.InitializeCompanyImplementation(ctx, log, cfg)
	if err != nil {
		log.Fatal().Msgf("unable to init company service, because:%v", err)
	}

	app.SetupRouter(func(ap *fiber.App) {
		ap.Use(
			requestid.New(),
			recover.New(),
		)

		companyRouter := ap.Group("/company")
		authMiddleware := server.AuthMiddleware(log, cfg.Secret.EncKey)
		company.Router(companyImp, companyRouter, authMiddleware)
	})
}
