package producer

import (
	"context"
	"encoding/json"
	"time"

	"gitlab.com/snarksliveshere/company-api/internal/config"
	"gitlab.com/snarksliveshere/company-api/internal/model"
	"gitlab.com/snarksliveshere/company-api/pkg/queue"
)

func (i *Implementation) StartProduceEvents(ctx context.Context, cfg *config.Producer) {
	for {
		time.Sleep(cfg.Delay)
		events, err := i.service.GetUnsentEvents(ctx, cfg.EventLimit)
		if err != nil {
			i.log.Error().Err(err).Msg("can't receive unsent companies")
			continue
		}

		ids := make([]int, 0, len(events))

		for _, v := range events {
			msg, err := getMessageFromEvent(v)
			if err != nil {
				i.log.Error().Err(err).Msg("can't get message from event")
				continue
			}
			err = i.queue.SendMessageInTopics(cfg.EventTopic, msg)
			if err != nil {
				i.log.Error().Err(err).Msgf("can't send message to topic:%s", cfg.EventTopic)
				continue
			}
			ids = append(ids, v.ID)
		}

		if len(ids) == 0 {
			continue
		}
		err = i.service.SetEventsProduced(ctx, ids)
		if err != nil {
			i.log.Error().Err(err).Msg("can't set events produced")
			continue
		}
	}
}

func getMessageFromEvent(event model.Event) (queue.Message, error) {
	b, err := json.Marshal(event)
	if err != nil {
		return queue.Message{}, err
	}
	return queue.Message{
		Key:   event.EntityName,
		Value: b,
	}, err
}
