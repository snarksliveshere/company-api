package producer

import (
	"github.com/rs/zerolog"

	"gitlab.com/snarksliveshere/company-api/internal/model/interfaces"
	"gitlab.com/snarksliveshere/company-api/pkg/queue"
)

// Implementation describe producer implementation struct
type Implementation struct {
	log     zerolog.Logger
	service interfaces.StorageEvent
	queue   queue.Queue
}

// New returns the producer implementation
func New(log zerolog.Logger, service interfaces.StorageEvent, queue queue.Queue) *Implementation {
	return &Implementation{
		log:     log,
		service: service,
		queue:   queue,
	}
}
