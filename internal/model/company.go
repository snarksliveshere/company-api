package model

// Company DB struct for company
type Company struct {
	ID              string `db:"id"`
	Name            string `db:"name"`
	Description     string `db:"description"`
	AmountEmployees uint32 `db:"amount_employees"`
	Registered      bool   `db:"registered"`
	CompanyType     string `db:"type"`
}
