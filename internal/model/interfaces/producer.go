package interfaces

import (
	"context"

	"gitlab.com/snarksliveshere/company-api/internal/model"
)

// StorageEvent interface return access to event storage layers
type StorageEvent interface {
	// GetUnsentEvents receives events with produced status false
	GetUnsentEvents(ctx context.Context, limit uint64) ([]model.Event, error)
	// SetEventsProduced sets produced status to true by ids
	SetEventsProduced(ctx context.Context, ids []int) error
}
