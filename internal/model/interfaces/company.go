package interfaces

import (
	"context"

	"gitlab.com/snarksliveshere/company-api/internal/model"
)

// ServiceCompany interface return access to company storage layers
type ServiceCompany interface {
	CreateCompany(ctx context.Context, company model.Company) error
	PatchCompany(ctx context.Context, company model.Company) error
	DeleteCompany(ctx context.Context, uuid string) error
	GetCompany(ctx context.Context, uuid string) (model.Company, error)
}
