package model

import "time"

// Event struct for event log
type Event struct {
	ID         int       `db:"id" json:"id"`
	Action     string    `db:"action" json:"action"`
	EntityName string    `db:"entity_name" json:"entity_name"`
	EntityID   string    `db:"entity_id" json:"entity_id"`
	Data       string    `db:"data" json:"data"`
	Produced   bool      `db:"produced" json:"-"`
	CreatedAt  time.Time `db:"created_at" json:"-"`
}
