package bootstrap

import (
	"context"
	"fmt"
	"sync"

	"github.com/Shopify/sarama"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/rs/zerolog"

	"gitlab.com/snarksliveshere/company-api/internal/config"
	"gitlab.com/snarksliveshere/company-api/internal/model/interfaces"
	"gitlab.com/snarksliveshere/company-api/internal/storage/pg"
	"gitlab.com/snarksliveshere/company-api/internal/storage/pg/decorator/company"
	"gitlab.com/snarksliveshere/company-api/pkg/queue"
)

var poolOnce = sync.Once{}
var pool *pgxpool.Pool

// SetupConnectionPool create db connection pool. Also used in test
func SetupConnectionPool(ctx context.Context, cfg *config.PG) (*pgxpool.Pool, error) {
	connStr := fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s?sslmode=disable",
		cfg.User, cfg.Password, cfg.Host, cfg.Port, cfg.Database)
	cfgPG, err := pgxpool.ParseConfig(connStr)
	if err != nil {
		return nil, fmt.Errorf("cant parse URI, because:%v", err)
	}
	cfgPG.ConnConfig.ConnectTimeout = cfg.Timeout
	cfgPG.MaxConns = cfg.MaxConns
	cfgPG.MinConns = cfg.MinConns
	poolOnce.Do(func() {
		pool, err = pgxpool.NewWithConfig(ctx, cfgPG)
	})
	return pool, err
}

func newCompanyEventLogDecorator(pool *pgxpool.Pool) interfaces.ServiceCompany {
	return company.NewEventLogDecorator(pool)
}

var kafkaClientOnce = sync.Once{}
var kafkaClient sarama.Client

func initKafkaClient(log zerolog.Logger, cfg *config.Kafka) sarama.Client {
	opts := getKafkaOptions(cfg)

	var err error
	kafkaClientOnce.Do(func() {
		kafkaClient, err = sarama.NewClient(cfg.Brokers, opts)
		if err != nil {
			log.Fatal().Msg(fmt.Sprintf("unable to init kafka: %v", err))
		}
	})

	return kafkaClient
}

func newKafkaProducer(client sarama.Client) (queue.Queue, error) {
	return queue.NewSyncProducer(client)
}

func getKafkaOptions(cfg *config.Kafka) *sarama.Config {
	options := sarama.NewConfig()
	options.Version = sarama.V2_0_0_0
	options.ClientID = cfg.ClientID
	options.Producer.Return.Successes = true
	options.Producer.Return.Errors = true
	options.Producer.RequiredAcks = sarama.WaitForAll
	options.Producer.Retry.Max = cfg.MaxRetries
	if cfg.SASLEnable {
		options.Net.SASL.Enable = true
		options.Net.SASL.User = cfg.Username
		options.Net.SASL.Password = cfg.Password
	}
	return options
}

func newProducerStorage(pool *pgxpool.Pool) interfaces.StorageEvent {
	return pg.NewStore(pool)
}
