//go:build wireinject

package bootstrap

import (
	"context"

	"github.com/google/wire"
	"github.com/rs/zerolog"

	"gitlab.com/snarksliveshere/company-api/internal/app/api/v1/company"
	"gitlab.com/snarksliveshere/company-api/internal/app/producer"
	"gitlab.com/snarksliveshere/company-api/internal/config"
)

var dbSet = wire.NewSet(
	SetupConnectionPool,
)

var companyImplSet = wire.NewSet(
	config.GetPGConfig,
	dbSet,
	newCompanyEventLogDecorator,
	company.New,
)

var producerImplSet = wire.NewSet(
	config.GetPGProducerConfig,
	dbSet,
	newProducerStorage,

	config.GetKafkaConfig,
	initKafkaClient,
	newKafkaProducer,

	producer.New,
)

func InitializeCompanyImplementation(context.Context, zerolog.Logger, *config.Config) (*company.Implementation, error) {
	wire.Build(companyImplSet)
	return &company.Implementation{}, nil
}

func InitializeProducer(context.Context, zerolog.Logger, *config.ProducerConfig) (*producer.Implementation, error) {
	wire.Build(producerImplSet)
	return &producer.Implementation{}, nil
}
