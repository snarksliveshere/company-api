CREATE OR REPLACE FUNCTION update_timestamp()
    RETURNS TRIGGER AS
'
    BEGIN
        NEW.updated_at = now();
        RETURN NEW;
    END;
'
    LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_time_fields(table_name text) RETURNS VOID
    LANGUAGE plpgsql
    AS
        $$
DECLARE
trigger_name TEXT;
BEGIN
EXECUTE 'ALTER TABLE ' || table_name || ' ADD COLUMN created_at timestamp WITH TIME ZONE DEFAULT NOW() NOT NULL;';
EXECUTE 'ALTER TABLE ' || table_name || ' ADD COLUMN updated_at timestamp WITH TIME ZONE DEFAULT NOW() NOT NULL;';
trigger_name := 't_' || replace(table_name, '.', '_') || '_upt';

EXECUTE 'CREATE TRIGGER ' || trigger_name || ' BEFORE UPDATE ON ' || table_name ||
        ' FOR EACH ROW EXECUTE PROCEDURE update_timestamp()';
END;
$$;


CREATE TYPE company_type AS ENUM ('Corporations','NonProfit','Cooperative','Proprietorship');

CREATE TABLE IF NOT EXISTS company
(
    id               UUID
    CONSTRAINT group_pkey
    PRIMARY KEY                 NOT NULL,
    name             VARCHAR(15) UNIQUE NOT NULL,
    description      VARCHAR(3000)      NOT NULL DEFAULT '',
    amount_employees INT                NOT NULL,
    registered       BOOL               NOT NULL,
    type             company_type       NOT NULL
    );

SELECT add_time_fields('company');

CREATE UNIQUE INDEX company_unique ON company (name);

CREATE TABLE IF NOT EXISTS event_log
(
    id          BIGSERIAL PRIMARY KEY,
    action      TEXT        NOT NULL,
    entity_name TEXT        NOT NULL,
    entity_id   TEXT        NOT NULL,
    data        JSONB NOT NULL DEFAULT '{}',
    produced    BOOLEAN     NOT NULL DEFAULT FALSE,
    created_at  TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
